import React, { memo } from 'react';


const LoadingComponent = memo(() => {
	return (
		<div>Loading...</div>
	);
});

export default LoadingComponent;
