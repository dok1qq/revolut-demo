import React, { memo, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

import { Button, NumberInput, Select, Text } from '@mantine/core';

import { Stack, Cluster } from '../elements';

import { Exchange } from '../../models/Exchange';
import { Account } from '../../models/Account';
import { State } from '../../models/Model';

import { useAccounts } from '../../contexts/AccountsContext';
import { convert, getPointRate, reverseConvert } from '../../utils/money';
import { ExchangeRates } from '../../models/ExchangeRates';
import { useQuery } from '../../hooks/useQuery';

import PaneComponent from '../pane/Pane';
import { useRates } from '../../contexts/RatesContext';

const ExchangeComponent = memo(() => {
	const query = useQuery();
	const navigate = useNavigate();

	const accountsContext = useAccounts();
	const ratesModel = useRates();

	const [exchange, setExchange] = useState<Exchange>({
		amount: 0,
		convertedAmount: 0,
		from: accountsContext.getAccount(query.get('from') || 'EUR') as Account,
		to: accountsContext.getAccount('USD') as Account,
		pointRate: '',
	});

	useEffect(() => {
		if (ratesModel.state === State.READY) {
			const {
				from,
				to,
			} = exchange;

			setExchange({
				...exchange,
				pointRate: getPointRate(from.currency, to.currency, ratesModel.data as ExchangeRates)
			});
		}
	}, [ratesModel]);

	const changeFromCurrency = (code: string | null): void => {
		if (!code) {
			return;
		}

		if (ratesModel.state !== State.READY) {
			return;
		}

		const newAccount: Account | null = accountsContext.getAccount(code);

		if (!newAccount) {
			return;
		}

		const {
			to,
		} = exchange;

		setExchange({
			amount: 0,
			to,
			from: newAccount,
			convertedAmount: convert(0, newAccount.currency, to.currency, ratesModel.data as ExchangeRates),
			pointRate: getPointRate(newAccount.currency, to.currency, ratesModel.data as ExchangeRates),
		});
	};

	const changeToCurrency = (code: string | null): void => {
		if (!code) {
			return;
		}

		if (ratesModel.state !== State.READY) {
			return;
		}

		const newAccount: Account | null = accountsContext.getAccount(code);

		if (!newAccount) {
			return;
		}

		const {
			amount,
			from,
		} = exchange;

		setExchange({
			amount,
			from,
			to: newAccount,
			convertedAmount: convert(amount, from.currency, newAccount.currency, ratesModel.data as ExchangeRates),
			pointRate: getPointRate(from.currency, newAccount.currency, ratesModel.data as ExchangeRates),
		});
	};

	const changeFromAmount = (amount: number): void => {
		if (ratesModel.state !== State.READY) {
			return;
		}

		const {
			from,
			to,
		} = exchange;

		setExchange({
			from,
			to,
			amount,
			convertedAmount: convert(amount, from.currency, to.currency, ratesModel.data as ExchangeRates),
			pointRate: getPointRate(from.currency, to.currency, ratesModel.data as ExchangeRates),
		});
	};

	const changeToAmount = (convertedAmount: number): void => {
		if (ratesModel.state !== State.READY) {
			return;
		}

		const {
			from,
			to,
		} = exchange;

		setExchange({
			from,
			to,
			convertedAmount,
			amount: reverseConvert(convertedAmount, from.currency, to.currency, ratesModel.data as ExchangeRates),
			pointRate: getPointRate(from.currency, to.currency, ratesModel.data as ExchangeRates),
		});
	};

	const onSellHandler = (): void  => {
		const {
			from,
			to,
			amount,
			convertedAmount,
		} = exchange;

		accountsContext.removeAmountById(from.id, amount);
		accountsContext.addAmountById(to.id, convertedAmount);

		navigate(`/accounts/${from.id}`);
	};

	const navigateToAccounts = (): void => {
		navigate(`/accounts`);
	};

	const availableCurrencies = () => {
		return accountsContext.getAccounts().map(({ currency, amount }: Account) => {
			const label: string = `${currency.code} - ${currency.name}`;
			return { value: currency.code, label, description: `Balance: ${amount}` };
		});
	};

	const validate = (): boolean => {
		if (Number(exchange.amount) > exchange.from.amount) {
			return true;
		}

		if (exchange.from.currency.code === exchange.to.currency.code) {
			return true;
		}

		if (!exchange.amount) {
			return true;
		}

		return false;
	};

	return (
		<PaneComponent title="Exchange">
			<form onSubmit={onSellHandler}>
				<Stack>
					<Text color="orange" size="lg">{exchange.pointRate}</Text>

					<Cluster>
						<Select
							label="From"
							data={availableCurrencies()}
							value={exchange.from.currency.code}
							onChange={(value: string | null) => changeFromCurrency(value)}
							labelProps={{ style: { color: 'var(--color-title)' } }}
							data-testid="from-select"
						/>
						<NumberInput
							type="text"
							label="Amount"
							min={0}
							max={1000000}
							defaultValue={0}
							step={10}
							precision={2}
							value={exchange.amount}
							onChange={(value: number | undefined) => changeFromAmount(value || 0)}
							labelProps={{ style: { color: 'var(--color-title)' } }}
							data-testid="from-amount"
						/>
						<NumberInput
							label="Balance"
							disabled
							value={exchange.from.amount}
							labelProps={{ style: { color: 'var(--color-title)' } }}
							precision={2}
							data-testid="from-balance"
						/>
					</Cluster>

					<Cluster>
						<Select
							label="To"
							data={availableCurrencies()}
							value={exchange.to.currency.code}
							onChange={(value: string | null) => changeToCurrency(value)}
							labelProps={{ style: { color: 'var(--color-title)' } }}
							data-testid="to-select"
						/>
						<NumberInput
							label="Amount"
							value={exchange.convertedAmount}
							min={0}
							step={10}
							precision={2}
							onChange={(value: number | undefined) => changeToAmount(value || 0)}
							labelProps={{ style: { color: 'var(--color-title)' } }}
							data-testid="to-amount"
						/>
						<NumberInput
							label="Balance"
							disabled
							value={exchange.to.amount}
							labelProps={{ style: { color: 'var(--color-title)' } }}
							precision={2}
							data-testid="to-balance"
						/>
					</Cluster>

					<Cluster>
						<Button data-testid="submit" color="teal" type="submit" disabled={validate()}>Sell</Button>
						<Button data-testid="accounts" type="button" onClick={navigateToAccounts}>Accounts</Button>
					</Cluster>
				</Stack>
			</form>
		</PaneComponent>
	);
});

export default ExchangeComponent;
