import React from 'react';
import { HashRouter as Router } from 'react-router-dom';
import { render, fireEvent, act } from '@testing-library/react';

import Exchange from './Exchange';

import { Account } from '../../models/Account';
import { RatesProvider } from '../../contexts/RatesContext';
import { AccountsService } from '../../services/AccountsService';
import { AccountsProvider } from '../../contexts/AccountsContext';
import { RatesService } from '../../services/RatesService';
import { ExchangeRates } from '../../models/ExchangeRates';
import { Observable, of } from 'rxjs';
import userEvent from '@testing-library/user-event';

// window.ResizeObserver =
// 	window.ResizeObserver ||
// 	jest.fn().mockImplementation(() => ({
// 		disconnect: jest.fn(),
// 		observe: jest.fn(),
// 		unobserve: jest.fn(),
// 	}));

const accountsService: AccountsService = new AccountsService();

accountsService.createAccount('USD', 0);
accountsService.createAccount('EUR', 1000);

const exchangeRates: ExchangeRates = {
	timestamp: Date.now(),
	base: 'USD',
	rates: {
		"USD": 1,
		"EUR": 2,
		"GBP": 4,
		"RUB": 100,
	},
}

class MockRates extends RatesService<ExchangeRates> {

	constructor() {
		super(1000);
	}

	protected request<P>(params: P | undefined): Observable<ExchangeRates> {
		return of(exchangeRates);
	}
}

const ratesService: RatesService<ExchangeRates> = new MockRates();

const mockedUsedNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
	...jest.requireActual('react-router-dom') as any,
	useNavigate: () => mockedUsedNavigate,
	useLocation: () => ({
		pathname: "localhost:3000/#/exchange?from=EUR"
	})
}));

describe('Exchange', () => {
	it('exchange EUR to USD', (done) => {
		const element = (
			<AccountsProvider service={accountsService}>
				<RatesProvider service={ratesService}>
					<Router>
						<Exchange />
					</Router>
				</RatesProvider>
			</AccountsProvider>
		);

		const utils = render(element);
		const fromSelect = utils.getByTestId('from-select');
		const fromAmount = utils.getByTestId('from-amount');
		const fromBalance = utils.getByTestId('from-balance');

		const toSelect = utils.getByTestId('to-select');
		const toAmount = utils.getByTestId('to-amount');
		const toBalance = utils.getByTestId('to-balance');

		const submit = utils.getByTestId('submit');

		// fireEvent.change(fromSelect, { target: { value: 'EUR' }});
		// fireEvent.change(toSelect, { target: { value: 'USD' }});


		userEvent.type(fromAmount, '100'); // stuck here, seems like a bug into component from ui-kit

		// fireEvent.submit(submit);
		//
		// const usdAccount: Account = accountsService.getAccount('USD') as Account;
		// const eurAccount: Account = accountsService.getAccount('EUR') as Account;
		//
		// expect(usdAccount.amount).toEqual(200);
		// expect(eurAccount.amount).toEqual(900);
		done();
	});
});
