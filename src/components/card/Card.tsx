import React from 'react';
import styled from '@emotion/styled';

const CardWrapper = styled.div`
	display: flex;
	width: 100%;
	cursor: pointer;
	padding: 8px;
	border-radius: 4px;
	background-color: var(--color-background);
	transition: background-color 0.2s ease-in-out;

	> * + * { margin-left: 10px; }

	&:hover { background-color: var(--color-pane-head); }
`;

const CardIcon = styled.div`
	display: flex;
	flex-shrink: 0;
	width: 36px;
	height: 36px;
	border-radius: 50%;
	background-color: lightseagreen;
`;

const CardBody = styled.div`
	display: flex;
	justify-content: space-between;
	width: 100%;
`;

const Description = styled.div`
	display: flex;
	flex-direction: column;
`;

const Point = styled.div`
	color: var(--color-white);
`;

const Title = styled.div`
	font-size: 14px;
	color: var(--color-title);

	&:hover { color: var(--color-white); }
`;

const Subtitle = styled.div`
	font-size: 12px;
	color: var(--color-subtitle);
`;

interface CardProps {
	title: string;
	subtitle: string;
	point: string;
	onClick?(): void;
}

const CardComponent = ({ title, subtitle, point, onClick }: CardProps) => (
	<CardWrapper onClick={onClick} className="card-wrapper">
		<CardIcon />
		<CardBody>
			<Description>
				<Title className="card-title">{ title }</Title>
				<Subtitle className="card-subtitle">{ subtitle }</Subtitle>
			</Description>
			<Point className="card-point">{ point }</Point>
		</CardBody>
	</CardWrapper>
);

export default CardComponent;
