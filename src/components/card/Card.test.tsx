import React from 'react';
import { render, unmountComponentAtNode } from 'react-dom';
import { act } from 'react-dom/test-utils';

import CardComponent from './Card';

let container: HTMLDivElement | null = null;
beforeEach(() => {
	container = document.createElement("div");
	document.body.appendChild(container);
});

afterEach(() => {
	if (container) {
		unmountComponentAtNode(container);
		container.remove();
		container = null;
	}
});

describe('CardComponent tests', () => {
	it("renders with all required props", () => {
		const title: string = 'USD';
		const subtitle: string = 'US Dollar';
		const point: string = '100';

		act(() => {
			render(<CardComponent title={title} subtitle={subtitle} point={point} />, container);
		});

		if (container) {
			const titleEl: HTMLDivElement | null = container.querySelector('.card-title');
			if (titleEl) {
				expect(titleEl.textContent).toBe(title);
			} else {
				fail('doesn\'t have title element');
			}

			const subtitleEl: HTMLDivElement | null = container.querySelector('.card-subtitle');
			if (subtitleEl) {
				expect(subtitleEl.textContent).toBe(subtitle);
			} else {
				fail('doesn\'t have subtitle element');
			}

			const pointEl: HTMLDivElement | null = container.querySelector('.card-point');
			if (pointEl) {
				expect(pointEl.textContent).toBe(point);
			} else {
				fail('doesn\'t have point element');
			}
		}
	});

	it('call onClick props', () => {
		const fn: () => void = jest.fn();

		act(() => {
			render(<CardComponent title={''} subtitle={''} point={''} onClick={fn} />, container);
		});

		if (container) {
			const wrapper: HTMLDivElement | null = container.querySelector('.card-wrapper');
			if (wrapper) {
				wrapper.click();
				expect(fn).toBeCalled();
			} else {
				fail('doesn\'t have wrapper element');
			}
		}
	});
});
