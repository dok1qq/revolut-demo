import React, { memo } from 'react';
import { NavigateFunction, useNavigate } from 'react-router-dom';

import { Button } from '@mantine/core';

import { Stack } from '../elements';

import { Account } from '../../models/Account';
import { useAccounts } from '../../contexts/AccountsContext';
import PaneComponent from '../pane/Pane';
import CardComponent from '../card/Card';

const AccountsComponent = memo(() => {
	const navigate: NavigateFunction = useNavigate();
	const accounts = useAccounts();

	const addAccountHandler = (): void => {
		navigate('/accounts/new');
	};

	const previewAccount = (id: string): void => {
		navigate(`/accounts/${id}`);
	};

	const renderAccounts = (): Array<React.ReactNode> => {
		return accounts.getAccounts().map((acc: Account) => (
			<CardComponent
				key={acc.id}
				title={acc.currency.name}
				subtitle={acc.currency.code}
				point={String(acc.amount)}
				onClick={() => previewAccount(acc.id)}
			/>
		));
	};

	return (
		<PaneComponent title="Accounts">
			<Stack>
				{renderAccounts()}
				<Button color="teal" onClick={addAccountHandler}>Create Account</Button>
			</Stack>
		</PaneComponent>
	);
});

export default AccountsComponent;
