import React, { memo, useState } from 'react';
import { useNavigate } from 'react-router-dom';

import { Button, Select } from '@mantine/core';

import { Currency } from '../../models/Currency';
import { Currencies } from '../../utils/currencies';
import { useAccounts } from '../../contexts/AccountsContext';

import { Stack, Cluster } from '../elements';
import PaneComponent from '../pane/Pane';

const AddAccountComponent = memo(() => {
	const navigate = useNavigate();
	const accountsService = useAccounts();

	const [currencyCode, setCurrencyCode] = useState<string | null>(null);

	const onSubmitHandler = (): void  => {
		if (currencyCode) {
			accountsService.createAccount(currencyCode);
			navigate('/accounts');
		}
	};

	const onSelectHandler = (value: string | null): void => {
		setCurrencyCode(value);
	};

	const accountsHandle = () => {
		navigate('/accounts');
	};

	const availableCurrencies = () => {
		return Currencies
			.filter((currency: Currency) => !accountsService.hasAccount(currency.code))
			.map((currency: Currency) => {
				const label: string = `${currency.code} - ${currency.name}`;
				return { label, value: currency.code };
			});
	};

	const validate = () => {
		if (currencyCode === null) {
			return true;
		}

		return false;
	}

	return (
		<PaneComponent title="Add Account">
			<form onSubmit={onSubmitHandler}>
				<Stack>
					<Select
						placeholder="Select currency"
						data={availableCurrencies()}
						value={currencyCode}
						onChange={onSelectHandler}
						aria-label="select-account"
					/>
					<Cluster>
						<Button
							type="submit"
							color="teal"
							aria-label="create-account"
							disabled={validate()}
						>
							Create Selected Account
						</Button>
						<Button aria-label="accounts" onClick={accountsHandle}>Accounts</Button>
					</Cluster>
				</Stack>
			</form>
		</PaneComponent>
	);
});

export default AddAccountComponent;
