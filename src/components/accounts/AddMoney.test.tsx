import React from 'react';
import { HashRouter as Router } from 'react-router-dom';
import { render, fireEvent } from '@testing-library/react';

import AddMoneyComponent from './AddMoney';
import { AccountsProvider } from '../../contexts/AccountsContext';
import { AccountsService } from '../../services/AccountsService';
import { Account } from '../../models/Account';

const service: AccountsService = new AccountsService();
service.createAccount('USD', 0);
const mockAccount: Account = service.getAccount('USD') as Account;

const mockedUsedNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
	...jest.requireActual('react-router-dom') as any,
	useNavigate: () => mockedUsedNavigate,
	useParams: () => ({
		id: mockAccount.id,
	}),
}));

beforeEach(() => {
	service.setAmountById(mockAccount.id, 0);
});

describe('AddMoneyComponent', () => {
	it('correctly set money in component', () => {
		const element = (
			<AccountsProvider service={service}>
				<Router>
					<AddMoneyComponent />
				</Router>
			</AccountsProvider>
		);

		const utils = render(element);
		const input = utils.getByLabelText('amount') as HTMLInputElement;

		expect(input.value).toEqual('0');

		fireEvent.change(input, { target: { value: '111' } });

		expect(input.value).toEqual('111');
	});

	it('can add money', () => {
		const element = (
			<AccountsProvider service={service}>
				<Router>
					<AddMoneyComponent />
				</Router>
			</AccountsProvider>
		);

		const utils = render(element);

		const accountId: string = mockAccount.id;
		const account: Account | null = service.getAccountById(accountId);

		const input = utils.getByLabelText('amount');
		const add = utils.getByLabelText('add-money');

		if (account) {
			expect(account.amount).toEqual(0);

			fireEvent.change(input, { target: { value: '100' } });
			fireEvent.submit(add);

			expect(account.amount).toEqual(100);
		}
	});
});
