import React from 'react';
import { HashRouter as Router } from 'react-router-dom';
import { render } from '@testing-library/react';

import { AccountsService } from '../../services/AccountsService';
import { AccountsProvider } from '../../contexts/AccountsContext';

import Accounts from './Accounts';

const mockedUsedNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
	...jest.requireActual('react-router-dom') as any,
	useNavigate: () => mockedUsedNavigate,
}));


describe('AccountsComponent', () => {
	it('can view all account', () => {
		const service: AccountsService = new AccountsService();
		service.createAccount('USD', 100);
		service.createAccount('EUR', 333);
		service.createAccount('GBP', 222);

		const element = (
			<AccountsProvider service={service}>
				<Router>
					<Accounts />
				</Router>
			</AccountsProvider>
		);

		const utils = render(element);

		expect(service.getAccounts().length).toEqual(3);

		const elements: NodeListOf<HTMLElement> = utils.container.querySelectorAll('.card-wrapper');
		expect(elements.length).toEqual(3);
	});
});
