import React, { memo, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';

import { Button, NumberInput } from '@mantine/core';

import PaneComponent from '../pane/Pane';
import { useAccounts } from '../../contexts/AccountsContext';

import { Stack, Cluster } from '../elements';

const AddMoneyComponent = memo(() => {
	const navigate = useNavigate();
	const params = useParams();
	const accountsService = useAccounts();

	const [amount, setAmount] = useState<number>(0);

	if (!params.id) {
		navigate(`/accounts`);
	}

	const onSubmitHandler = (): void  => {
		let added: boolean = false;

		if (params.id) {
			added = accountsService.addAmountById(params.id, amount);
		}

		if (added) {
			navigate(`/accounts/${params.id}`);
		}
	};

	const accountsHandle = () => {
		navigate('/accounts');
	};

	const validate = () => {
		if (!amount) {
			return true;
		}

		return false;
	};

	return (
		<PaneComponent title="Add Money">
			<form onSubmit={onSubmitHandler}>
				<Stack>
					<NumberInput
						min={0}
						max={1000000}
						step={10}
						defaultValue={0}
						placeholder="Set Amount"
						value={amount}
						onChange={(value: number | undefined) => setAmount(value || 0)}
						aria-label="amount"
					/>
					<Cluster>
						<Button aria-label="add-money" color="teal" disabled={validate()} type="submit">Add Money</Button>
						<Button aria-label="accounts" onClick={accountsHandle}>Accounts</Button>
					</Cluster>
				</Stack>
			</form>
		</PaneComponent>
	);
});

export default AddMoneyComponent;
