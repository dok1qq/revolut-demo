import React from 'react';
import { HashRouter as Router } from 'react-router-dom';
import { render } from '@testing-library/react';

import AddAccountComponent from './AddAccount';
import { AccountsProvider } from '../../contexts/AccountsContext';
import { AccountsService } from '../../services/AccountsService';

const mockedUsedNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
	...jest.requireActual('react-router-dom') as any,
	useNavigate: () => mockedUsedNavigate,
}));

window.ResizeObserver =
	window.ResizeObserver ||
	jest.fn().mockImplementation(() => ({
		disconnect: jest.fn(),
		observe: jest.fn(),
		unobserve: jest.fn(),
	}));

describe('AddMoneyComponent', () => {
	it('can add account', () => {
		const service: AccountsService = new AccountsService();

		const element = (
			<AccountsProvider service={service}>
				<Router>
					<AddAccountComponent />
				</Router>
			</AccountsProvider>
		);

		const utils = render(element);

		expect(utils.container).toBeTruthy();
	});
});
