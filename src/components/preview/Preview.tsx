import React, { memo } from 'react';
import { useNavigate, useParams } from 'react-router-dom';

import { Button } from '@mantine/core';

import { Account } from '../../models/Account';
import { useAccounts } from '../../contexts/AccountsContext';
import PaneComponent from '../pane/Pane';
import CardComponent from '../card/Card';

import { Stack, Cluster } from '../elements';

const PreviewComponent = memo(() => {
	const params = useParams();
	const navigate = useNavigate();
	const accountsService = useAccounts();

	let account: Account | null = null;
	if (params.id) {
		account = accountsService.getAccountById(params.id);
	}

	const addMoneyHandle = () => {
		navigate(`/accounts/${params.id}/add`);
	};

	const exchangeHandle = () => {
		if (account) {
			navigate(`/exchange?from=${account.currency.code}`);
		}
	};

	const accountsHandle = () => {
		navigate('/accounts');
	};

	const renderAccount = ({ amount, currency }: Account) => {
		return (
			<PaneComponent title="Account">
				<Stack>
					<CardComponent title={currency.name} subtitle={currency.code} point={String(amount)} />
					<Cluster>
						<Button color="teal" onClick={addMoneyHandle}>Add Money</Button>
						<Button color="orange" onClick={exchangeHandle}>Exchange</Button>
						<Button onClick={accountsHandle}>Accounts</Button>
					</Cluster>
				</Stack>
			</PaneComponent>
		);
	};

	if (account) {
		return renderAccount(account);
	}

	return null;
});

export default PreviewComponent;
