import React from 'react';
import styled from '@emotion/styled';

const PaneWrapper = styled.div`
	display: flex;
	flex-direction: column;
	width: 100%;
	border-radius: 4px;
	background-color: var(--color-pane-head);
`;

const PaneHead = styled.div`
	display: flex;
	align-items: center;
	padding: 10px 20px;
	width: 100%;
	height: 56px;
	font-size: 18px;
`;

const PaneBody = styled.div`
	display: flex;
	flex-direction: column;
	width: 100%;
	padding: 32px 20px;
	background-color: var(--color-pane-body);
`;

interface PaneProps {
	title: string;
	children: React.ReactNode;
}

const PaneComponent = ({ title, children }: PaneProps) => (
	<PaneWrapper>
		<PaneHead>{ title }</PaneHead>
		<PaneBody>{ children }</PaneBody>
	</PaneWrapper>
);

export default PaneComponent;
