import styled from '@emotion/styled';

export const Stack = styled.div`
	display: flex;
	flex-direction: column;

	& > * + * {
		margin-top: 16px;
	}
`;

export const Cluster = styled.div`
	display: flex;

	& > * { flex: 1; }
	& > * + * {
		margin-left: 8px;
	}
`;

export const Box = styled.div`
	padding: 16px;
`
