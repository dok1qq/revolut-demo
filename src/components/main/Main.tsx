import React, { lazy, memo, Suspense } from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';

const Exchange = lazy(() => import('../exchange/Exchange'));
const Accounts = lazy(() => import('../accounts/Accounts'));
const AddAccount = lazy(() => import('../accounts/AddAccount'));
const AddMoney = lazy(() => import('../accounts/AddMoney'));
const Preview = lazy(() => import('../preview/Preview'));

import Loading from '../../loading/Loading';
import AccountsGuard from '../application/AccountsGuard';
import ExchangeGuard from '../application/ExchangeGuard';

const MainComponent = memo(() => {
	return (
		<Suspense fallback={<Loading />}>
			<Routes>
				<Route path='accounts' element={<Accounts />} />
				<Route path='accounts/new' element={<AddAccount />} />
				<Route element={<ExchangeGuard />}>
					<Route path='exchange' element={<Exchange />} />
				</Route>
				<Route element={<AccountsGuard />} >
					<Route path='accounts/:id' element={<Preview />} />
					<Route path='accounts/:id/add' element={<AddMoney />} />
				</Route>
				<Route path="*" element={<Navigate to="/accounts" replace />} />
			</Routes>
		</Suspense>
	);
});

export default MainComponent;
