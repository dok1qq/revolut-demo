import React, { memo } from 'react';
import { Navigate, Outlet } from 'react-router-dom';
import { useAccounts } from '../../contexts/AccountsContext';
import { useQuery } from '../../hooks/useQuery';


const ExchangeGuard = memo(() => {
	const accounts = useAccounts();
	const query = useQuery();

	const from: string | null = query.get('from');
	if (from) {
		if (!accounts.hasAccount(from)) {
			return <Navigate to="/accounts" />;
		}
	}

	return <Outlet />;
});

export default ExchangeGuard;
