import React, { memo, Suspense } from 'react';
import { Routes, Route } from 'react-router-dom';

import Loading from '../../loading/Loading';
import MainComponent from '../main/Main';
import NotFoundComponent from '../404/404';

const Application = memo(() => {
	return (
		<Suspense fallback={<Loading />}>
			<Routes>
				{/* Login */}
				<Route path='404' element={<NotFoundComponent /> } />
				<Route path='*' element={<MainComponent />} />
			</Routes>
		</Suspense>
	);
});

export default Application;
