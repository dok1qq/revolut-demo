import React, { memo } from 'react';
import { Navigate, Outlet, useParams } from 'react-router-dom';
import { useAccounts } from '../../contexts/AccountsContext';
import { Account } from '../../models/Account';


const AccountsGuard = memo(() => {
	const accounts = useAccounts();
	const params = useParams();

	if (params.id) {
		const account: Account | null =  accounts.getAccountById(params.id);

		if (!account) {
			return <Navigate to="/accounts" />;
		}
	}

	return <Outlet />;
});

export default AccountsGuard;
