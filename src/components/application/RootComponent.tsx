import React from 'react';
import { HashRouter as Router } from 'react-router-dom';

import Application from './Application';
import { AccountsProvider } from '../../contexts/AccountsContext';
import { RatesProvider } from '../../contexts/RatesContext';

const RootComponent = () => {
	return (
		<AccountsProvider>
			<RatesProvider>
				<Router>
					<Application />
				</Router>
			</RatesProvider>
		</AccountsProvider>
	);
}

export default RootComponent;
