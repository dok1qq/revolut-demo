import { Rates } from './Rates';

export interface ExchangeRates {
	timestamp: number;
	base: string;
	rates: Rates;
}
