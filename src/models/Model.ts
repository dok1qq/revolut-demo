export enum State {
	PENDING = 'PENDING',
	READY = 'READY',
	ERROR = 'ERROR',
	EMPTY = 'EMPTY',
}

/*interface PendingState {
	state: State.PENDING;
}

interface EmptyState {
	state: State.EMPTY;
}

interface ErrorState {
	state: State.ERROR;
	message: string;
}

interface ReadyState<T> {
	state: State.READY;
	data: T;
}

export type Model<T> = PendingState | EmptyState | ErrorState | ReadyState<T>;*/

export interface Model<T> {
	data?: T;
	state: State;
}

