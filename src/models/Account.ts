import { Currency } from './Currency';

export interface Account {
	id: string;
	amount: number;
	currency: Currency;
}
