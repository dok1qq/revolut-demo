import { Account } from './Account';

export interface Exchange {
	amount: number;
	convertedAmount: number;
	from: Account;
	to: Account;
	pointRate: string;
}
