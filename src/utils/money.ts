import { Rates } from '../models/Rates';
import { Currency } from '../models/Currency';
import { ExchangeRates } from '../models/ExchangeRates';
import { roundNumber } from './round';

export function convert(amount: number, from: Currency, to: Currency, exchangeRates: ExchangeRates): number {
	const {
		base,
		rates,
	} = exchangeRates;

	return roundNumber(amount * getRate(to, from, base, rates));
}

export function reverseConvert(convertedAmount: number, from: Currency, to: Currency, exchangeRates: ExchangeRates): number {
	const {
		base,
		rates,
	} = exchangeRates;

	return roundNumber(convertedAmount / getRate(to, from, base, rates));
}

export function getPointRate(from: Currency, to: Currency, exchangeRates: ExchangeRates): string {
	const {
		base,
		rates,
	} = exchangeRates;

	return `1 ${from.code} = ${roundNumber(getRate(to, from, base, rates))} ${to.code}`;
}

function getRate(to: Currency, from: Currency, base: string, rates: Rates): number {

	if (!rates[to.code] || !rates[from.code]) {
		throw Error(`Unknown rate: ${to.code} or ${from.code}`);
	}

	if (from.code === base) {
		return rates[to.code];
	}

	if (to.code === base) {
		return 1 / rates[from.code];
	}

	return rates[to.code] * (1 / rates[from.code]);
}
