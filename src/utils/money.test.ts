import { ExchangeRates } from '../models/ExchangeRates';
import { CurrenciesMap } from './currencies';
import { Currency } from '../models/Currency';
import { convert, getPointRate, reverseConvert } from './money';

const exchangeRates: ExchangeRates = {
	timestamp: Date.now(),
	base: 'USD',
	rates: {
		"USD": 1,
		"EUR": 2,
		"GBP": 4,
		"RUB": 100,
	},
}

describe('Money', () => {
	it('convert USD to USD', () => {
		const usdCurrency: Currency | undefined = CurrenciesMap.get('USD');

		if (usdCurrency) {
			const result = convert(100, usdCurrency, usdCurrency, exchangeRates);
			expect(result).toEqual(100);
		}
	});

	it('convert USD to EUR', () => {
		const usdCurrency: Currency | undefined = CurrenciesMap.get('USD');
		const eurCurrency: Currency | undefined = CurrenciesMap.get('EUR');

		if (usdCurrency && eurCurrency) {
			const result = convert(100, usdCurrency, eurCurrency, exchangeRates);
			expect(result).toEqual(200);
		}
	});

	it('convert EUR to USD', () => {
		const usdCurrency: Currency | undefined = CurrenciesMap.get('USD');
		const eurCurrency: Currency | undefined = CurrenciesMap.get('EUR');

		if (usdCurrency && eurCurrency) {
			const result = convert(100, eurCurrency, usdCurrency, exchangeRates);
			expect(result).toEqual(50);
		}
	});

	it('convert EUR to GBP', () => {
		const eurCurrency: Currency | undefined = CurrenciesMap.get('EUR');
		const gbpCurrency: Currency | undefined = CurrenciesMap.get('GBP');

		if (eurCurrency && gbpCurrency) {
			const result = convert(100, eurCurrency, gbpCurrency, exchangeRates);
			expect(result).toEqual(200);
		}
	});

	it('reverseConvert EUR to GBP', () => {
		const usdCurrency: Currency | undefined = CurrenciesMap.get('USD');
		const eurCurrency: Currency | undefined = CurrenciesMap.get('EUR');

		if (eurCurrency && usdCurrency) {
			const result = reverseConvert(200, usdCurrency, eurCurrency, exchangeRates);
			expect(result).toEqual(100);
		}
	});

	it('getPointRate EUR to USD', () => {
		const usdCurrency: Currency | undefined = CurrenciesMap.get('USD');
		const eurCurrency: Currency | undefined = CurrenciesMap.get('EUR');

		if (eurCurrency && usdCurrency) {
			const result = getPointRate(eurCurrency, usdCurrency, exchangeRates);
			expect(result).toBe('1 EUR = 0.5 USD');
		}
	});
});
