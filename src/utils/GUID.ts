import { v4 as uuidv4 } from 'uuid';

export class GUID {
	static generate(): string {
		return uuidv4();
	}
}
