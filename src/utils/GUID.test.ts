import { GUID } from './GUID';

describe('GUID', () => {
	it('generate string with correct pattern', () => {
		const id: string = GUID.generate();
		const pattern: RegExp = new RegExp('^[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{12}$');

		expect(id).toMatch(pattern);
	});
});
