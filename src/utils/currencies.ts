import { Currency } from '../models/Currency';
import CurrenciesJSON from '../assets/currencies.json';

export const Currencies = CurrenciesJSON as Currency[];

export const CurrenciesMap: Map<string, Currency> = new Map<string, Currency>(
	CurrenciesJSON.map((currency: Currency) => [currency.code, currency]),
);
