import React, { createContext, useContext, useEffect, useState } from 'react';
import { Subscription } from 'rxjs';

import { Model, State } from '../models/Model';
import { RatesService } from '../services/RatesService';
import { ExchangeRates } from '../models/ExchangeRates';
import { OpenExchangeRatesService } from '../services/OpenExchangeRatesService';

const ratesService: RatesService<ExchangeRates> = new OpenExchangeRatesService();

const RatesContext = createContext<RatesService<ExchangeRates>>(ratesService);

interface RatesProviderProps {
	service?: RatesService<ExchangeRates>;
	children: React.ReactNode | React.ReactNode[];
}

export const RatesProvider = ({ service, children }: RatesProviderProps) => {
	const value: RatesService<ExchangeRates> = service || ratesService;

	return (
		<RatesContext.Provider value={value}>
			{children}
		</RatesContext.Provider>
	);
};

export function useRates(): Model<ExchangeRates> {
	const context: RatesService<ExchangeRates> = useContext(RatesContext);
	const [ratesModel, setRatesModel] = useState<Model<ExchangeRates>>({ state: State.EMPTY });

	useEffect(() => {
		const subscription: Subscription = context.connect().subscribe({
			next: (model: Model<ExchangeRates>) => setRatesModel(model),
		});

		return () => {
			subscription.unsubscribe();
		};
	}, []);

	return ratesModel;
}
