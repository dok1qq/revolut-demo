import React, { createContext, useContext } from 'react';
import { AccountsService } from '../services/AccountsService';

const accountsService: AccountsService = new AccountsService();
accountsService.createAccount('USD');
accountsService.createAccount('EUR', 1500);
accountsService.createAccount('GBP', 500);

const AccountsContext = createContext<AccountsService>(accountsService);

export function useAccounts(): AccountsService {
	return useContext(AccountsContext);
}

interface AccountsProviderProps {
	service?: AccountsService;
	children: React.ReactNode | React.ReactNode[];
}

export const AccountsProvider = ({ service, children }: AccountsProviderProps) => {
	const value: AccountsService = service || accountsService;

	return (
		<AccountsContext.Provider value={value}>
			{children}
		</AccountsContext.Provider>
	);
};
