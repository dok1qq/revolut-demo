import { GUID } from '../utils/GUID';

import { CurrenciesMap } from '../utils/currencies';

import { Account } from '../models/Account';
import { Currency } from '../models/Currency';


export class AccountsService {

	private accounts: Account[] = [];
	private accountsCodeMap: Map<string, Account> = new Map();
	private accountsIdMap: Map<string, Account> = new Map();

	createAccount(code: string, amount: number = 0): Account {
		const currency: Currency | undefined = CurrenciesMap.get(code);

		if (currency === undefined) {
			throw Error(`Cannot create an account with code: ${code}`);
		}

		const account: Account = {
			id: GUID.generate(),
			amount: amount || 0,
			currency,
		};

		this.accounts.push(account);
		this.accountsCodeMap.set(code, account);
		this.accountsIdMap.set(account.id, account);

		return account;
	}

	hasAccount(code: string): boolean {
		return this.accountsCodeMap.has(code);
	}

	getAccount(code: string): Account | null {
		if (this.accountsCodeMap.has(code)) {
			return this.accountsCodeMap.get(code) as Account;
		}

		console.info(`Cannot get an account with code: ${code}.`);
		return null;
	}

	getAccountById(id: string): Account | null {
		const account: Account | undefined = this.accountsIdMap.get(id);

		if (account === undefined) {
			console.info(`Cannot get an account with id: ${id}.`);
			return null;
		}

		return account;
	}

	getAccounts(): Account[] {
		return this.accounts;
	}

	addAmountByCode(code: string, amount: number): boolean {
		const account: Account | undefined = this.accountsCodeMap.get(code);

		if (account === undefined) {
			console.info(`Cannot update an account with code: ${code}.`);
			return false;
		}

		account.amount = amount;
		return true;
	}

	addAmountById(id: string, amount: number): boolean {
		const account: Account | undefined = this.accountsIdMap.get(id);

		if (account === undefined) {
			console.info(`Cannot update an account with id: ${id}.`);
			return false;
		}

		account.amount += amount;
		return true;
	}

	setAmountById(id: string, amount: number): boolean {
		const account: Account | undefined = this.accountsIdMap.get(id);

		if (account === undefined) {
			console.info(`Cannot update an account with id: ${id}.`);
			return false;
		}

		account.amount = amount;
		return true;
	}


	removeAmountById(id: string, amount: number): boolean {
		const account: Account | undefined = this.accountsIdMap.get(id);

		if (account === undefined) {
			console.info(`Cannot update an account with id: ${id}.`);
			return false;
		}

		account.amount -= amount;
		return true;
	}
}
