import { Account } from '../models/Account';
import { AccountsService } from './AccountsService';

const pattern: RegExp = new RegExp('^[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{12}$');

describe('AccountsService', () => {
	it('create account', () => {
		const service: AccountsService = new AccountsService();
		expect(service.getAccounts().length).toEqual(0);

		const account: Account = service.createAccount('USD');

		expect(service.getAccounts().length).toEqual(1);
		expect(account).toBeTruthy();
		expect(account.id).toMatch(pattern);
		expect(account.currency).toBeTruthy();
		expect(account.currency.code).toBe('USD');
		expect(account.amount).toEqual(0);
	});

	it('create empty account', () => {
		const service: AccountsService = new AccountsService();
		service.createAccount('EUR');

		expect(service.getAccount('EUR')).toBeTruthy();

		const account: Account = service.getAccount('EUR') as Account;
		expect(account.amount).toEqual(0);
	});

	it('create account with some money', () => {
		const service: AccountsService = new AccountsService();
		service.createAccount('EUR', 100);

		expect(service.getAccount('EUR')).toBeTruthy();

		const account: Account = service.getAccount('EUR') as Account;
		expect(account.amount).toEqual(100);
	});

	it('correct check account', () => {
		const service: AccountsService = new AccountsService();
		service.createAccount('USD');

		expect(service.hasAccount('USD')).toBeTruthy();
	});

	it('correctly get account by id', () => {
		const service: AccountsService = new AccountsService();
		const createdAccount: Account = service.createAccount('USD', 100);
		const receivedAccount: Account | null = service.getAccountById(createdAccount.id);

		expect(receivedAccount).toBeTruthy();
		if (receivedAccount) {
			expect(createdAccount.id).toEqual(receivedAccount.id);
			expect(createdAccount.amount).toEqual(receivedAccount.amount);
			expect(createdAccount.currency.code).toEqual(receivedAccount.currency.code);
		}
	});

	it('correctly get account by code', () => {
		const service: AccountsService = new AccountsService();
		const createdAccount: Account = service.createAccount('USD', 100);
		const receivedAccount: Account | null = service.getAccount(createdAccount.currency.code);

		expect(receivedAccount).toBeTruthy();
		if (receivedAccount) {
			expect(createdAccount.id).toEqual(receivedAccount.id);
			expect(createdAccount.amount).toEqual(receivedAccount.amount);
			expect(createdAccount.currency.code).toEqual(receivedAccount.currency.code);
		}
	});

	it('correctly get accounts', () => {
		const service: AccountsService = new AccountsService();
		service.createAccount('USD', 100);
		expect(service.getAccounts().length).toEqual(1);

		service.createAccount('EUR', 200);
		expect(service.getAccounts().length).toEqual(2);

		service.createAccount('RUB', 200);
		expect(service.getAccounts().length).toEqual(3);
	});

	it('correctly add amount by code', () => {
		const service: AccountsService = new AccountsService();
		const account: Account = service.createAccount('USD', 0);
		service.addAmountByCode('USD', 100);

		expect(account.amount).toEqual(100);
	});

	it('correctly add amount by id', () => {
		const service: AccountsService = new AccountsService();
		const account: Account = service.createAccount('USD', 0);
		service.addAmountById(account.id, 100);

		expect(account.amount).toEqual(100);
	});

	it('correctly set amount by id', () => {
		const service: AccountsService = new AccountsService();
		const account: Account = service.createAccount('USD', 100);
		service.setAmountById(account.id, 200);

		expect(account.amount).toEqual(200);
	});

	it('correctly remove amount by id', () => {
		const service: AccountsService = new AccountsService();
		const account: Account = service.createAccount('USD', 100);
		service.removeAmountById(account.id, 50);

		expect(account.amount).toEqual(50);
	});
});
