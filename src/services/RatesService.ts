import { catchError, map, Observable, of, startWith, switchMap, tap, timer } from 'rxjs';

import { Model, State } from '../models/Model';

export abstract class RatesService<T> {

	private rates: T | undefined;

	protected constructor(private readonly interval: number = 3000) {}

	protected abstract request<P>(params?: P): Observable<T>;

	connect<P>(params?: P): Observable<Model<T>> {
		return timer(0, this.interval).pipe(
			switchMap(() => {
				return this.request(params).pipe(
					tap({ next: (data: T) => { this.rates = data; } }),
					map((data: T) => ({ state: State.READY, data })),
					catchError((err: unknown) => {
						console.error(err);
						return of({ state: State.ERROR });
					}),
					startWith({ state: State.PENDING })
				);
			}),
		);
	}

	getLatestRates(): T | undefined {
		return this.rates;
	}
}
