import { RatesService } from './RatesService';
import { Observable, of } from 'rxjs';
import { Model, State } from '../models/Model';
import DoneCallback = jest.DoneCallback;

class MockRates extends RatesService<string> {

	constructor(private str: string = 'Result string') {
		super(1000);
	}

	protected request<P>(params: P | undefined): Observable<string> {
		return of(this.str);
	}
}

describe('RatesService', () => {
	it('correct model behaviour', (done: DoneCallback) => {
		const service: RatesService<string> = new MockRates();

		service.connect().subscribe({
			next: (model: Model<string>) => {
				if (model.state === State.READY) {
					expect(model.data).toBeTruthy();
					expect(model.data).toBe('Result string');
					done();
				} else if (
					model.state === State.EMPTY
					|| model.state === State.PENDING
					|| model.state === State.ERROR
				) {
					expect(model.data).toEqual(undefined);
				}
			},
			error: () => {
				fail();
			},
			complete: () => {
				done();
			},
		});

		expect(service).toBeTruthy();
	}, 500);

	it('get another response after intreval', (done: DoneCallback) => {
		const service: RatesService<string> = new MockRates();
		let counter: number = 0;

		service.connect().subscribe({
			next: (model: Model<string>) => {
				if (model.state === State.READY) {
					expect(model.data).toBeTruthy();
					counter++;

					if (counter >= 2) {
						done();
					}
				} else if (
					model.state === State.EMPTY
					|| model.state === State.PENDING
					|| model.state === State.ERROR
				) {
					expect(model.data).toEqual(undefined);
				}
			},
			error: () => {
				fail();
			},
			complete: () => {
				done();
			},
		});

		expect(service).toBeTruthy();
	}, 1500);
});
