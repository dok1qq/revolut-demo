import { Observable, Subscriber } from 'rxjs';

import { RatesService } from './RatesService';
import { ExchangeRates } from '../models/ExchangeRates';
import { OpenExchangeRates } from '../models/OpenExchangeRates';

const APP_ID = '9ddb099f413c48e2937fe521c7b95be4';

export class OpenExchangeRatesService extends RatesService<ExchangeRates> {

	private readonly URL: string = `https://openexchangerates.org/api/latest.json?app_id=${APP_ID}&base=USD`;

	constructor() {
		super(10000);
	}

	protected request(): Observable<ExchangeRates> {
		return new Observable<ExchangeRates>((subscriber: Subscriber<ExchangeRates>) => {
			fetch(this.URL)
				.then(response => response.json())
				.then((data: OpenExchangeRates) => ({
					base: data.base,
					timestamp: data.timestamp,
					rates: data.rates,
				}))
				.then((data: ExchangeRates) => {
					subscriber.next(data);
					subscriber.complete();
				})
				.catch(err => subscriber.error(err));
		});
	}
}
