import React from 'react';
import ReactDOM from 'react-dom';
import { injectGlobal } from '@emotion/css';

import RootComponent from './components/application/RootComponent';

injectGlobal`
	@import url('https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap');
	html, body {
		display: block;
		font-family: Nunito,serif;
		margin: 0;
		padding:0;
		width: 100%;
		height: 100%;
		color: var(--color-title);
		background-color: var(--color-background);
	}
	
	* {
		box-sizing: border-box;
	}
	
	:root {
		--color-white: #fff;
		--color-dark: #000;
		--color-background: #1a1b20;
		--color-pane-head: #2b2c31;
		--color-pane-body: #26272c;
		--color-title: #d6d6d9;
		--color-subtitle: #8f8f91;
		--color-title-hover: #fff;
	}
	
	body {
		display: flex;
		justify-content: center;
		position: relative;
	}
	
	#app-root {
		margin: 64px 0;
		width: 500px;
	}
	
	@media screen and (max-width: 1000px) {
		#app-root {
			margin: 16px;
			width: 100%;
		}
	}
`

ReactDOM.render(
	<RootComponent />,
	document.getElementById('app-root'),
);

